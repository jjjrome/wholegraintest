Some notes on my approach and shortcuts taken in the interests of time.

## Front End Test:

Files are in a folder called Frontend.

- Used a basic gulp and sass boilerplate with Sass folder structure already setup along with normalize and some other things.
- Didn't import Bootstrap or similar as I thought it would zero out any visibility of my actual css ability. (it does mean that there are lots of basic styles missing that you'd want if this site were to scale or be in production.
- Didn't fuss about browser compatibility 
- Didn't worry about optimisations such as compressing all the assets etc.
- Didn't have time to make the menu work.
- Didn't have time to add any nice hover states etc. Would have been nice to


## Wordpress test:

- Used ACF for custom fields
- Added Ingredients (repeater), Difficulty level and recipe time
- On the homepage, click the Get Random Recipe button to get the title, difficulty and content (instructions).
- Didn't have time to show Ingredients, recipe time and category in this view.

The test took me just under 3 hours. I got held up a bit setting up the environments - my normal setup would have been overkill for both. I also got a bit held up on the wordpress API as I've only used it breifly before, and not v2.

## General Questions

### 1. Small animated (or not) interactions
Stuff like scroll locking, buttons clicks, opening modals,....SLIDERS! So many things have no relation to real world physics making them feel one step removed from the user. Coupled with the fact that these interactive elements are often placed with little thought given to usability, the user is left with deeply frustrating experiences. The animations and layouts end up being less usable than simply laying content out in the usual flow. 

I would always make sure that best practice UX principles are followed - with reputable research backing up these patterns where possible. Appropriate design patterns differ between desktop and mobile, so the pattern used should be able to switch at various breakpoints (look at instagram on the web browser for a bad example of simply scaling up a mobile design). Secondly, animations should be optimised so that they do not appear sluggish and 'real world' physics should be used so that interactions feel intuitive (David Desandro has spoken well on this).

### 2. There's a bug!
1. Try to reproduce their environment as closely as possible (browser, extensions, OS)
2. If possible, use the machine that experienced the bug and simplify as much as possible until the bug doesn't occur (clear caches, disable extensions, use a different browser)
3. Examine all logs (and introduce more logging if necessary)
4. Walk through the relevant code line by line.